#include "imagemPgm.hpp"

#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <sstream>

using namespace std;

#define MAX 150

imagemPgm::imagemPgm () //Constroe uma imagemPgm vazia
{
	altura = 0;
	largura = 0;
	nivelCinza = 0;
	pixels = NULL;
}

imagemPgm::imagemPgm (int altura, int largura, int nivelCinza)
{
	this->altura = altura;
	this->largura = largura;
	this->nivelCinza = nivelCinza;
	pixels = new int [largura*altura];
}

imagemPgm::imagemPgm (char nomeArquivo []) //Verifica a imagem
{
	char cabecalho [MAX];
	char * ptr;
    ifstream imagem;
	
	imagem.open(nomeArquivo, ios::in | ios::binary);

    if (!imagem)
    {
		cout << "Nao foi possivel abrir a imagem!!" << endl;
		exit (1);
    }

    imagem.getline(cabecalho,MAX,'\n');
    if ((cabecalho[0]!='P') || (cabecalho[1]!='5'))
    {   
		cout << "A referida imagem " << nomeArquivo << "nao e PGM" << endl;
		exit(1);
    }

    imagem.getline(cabecalho,MAX,'\n');    
    while(cabecalho[0]=='#')
    {
		imagem.getline(cabecalho,MAX,'\n');
	}
	
    altura = strtol(cabecalho,&ptr,0);
    largura = atoi(ptr);

    imagem.getline(cabecalho,MAX,'\n');
    nivelCinza = strtol(cabecalho,&ptr,0);
    pixels = new int[altura*largura];
    
    cout << "Imagem verificada com sucesso!" << endl;
}

int imagemPgm::getAltura ()
{
	return altura;
}

int imagemPgm::getLargura ()
{
	return largura;
}

int imagemPgm::getNivelCinza ()
{
	return nivelCinza;
}

void imagemPgm::setAltura (int altura)
{
	this->altura = altura;
}

void imagemPgm::setLargura (int largura)
{
	this->largura = largura;
}

void imagemPgm::setNivelCinza (int nivelCinza){
	this->nivelCinza = nivelCinza;
}

void imagemPgm::leiaImagem(char nomeArquivo []){
	int i,j;
	unsigned char * tamanhoPixels;
	char cabecalho[MAX];
	char *ptr;
	ifstream imagem;
	
	imagem.open(nomeArquivo, ios::in | ios::binary);		

	imagem.getline(cabecalho,MAX,'\n');

	imagem.getline(cabecalho,MAX,'\n');
	
	while(cabecalho[0]=='#')
	{
		imagem.getline(cabecalho,MAX,'\n');
	}

	altura = strtol(cabecalho,&ptr,0);
	largura = atoi(ptr);

	imagem.getline(cabecalho,MAX,'\n');

	nivelCinza = strtol(cabecalho,&ptr,0);
	
	tamanhoPixels = (unsigned char *) new unsigned char [altura*largura];
	
	imagem.read(reinterpret_cast<char *>(tamanhoPixels), (altura*largura)*sizeof(unsigned char));
	
	if(imagem.fail())
	{
		cout << "A imagem " << nomeArquivo << "tem tamanho errado!" << endl;
		exit(1);
	}
	
	imagem.close();
	
	int val; 
	
	for(i=0; i<altura; i++)
	{
		for(j=0; j<largura; j++)
		{
			val = (int)tamanhoPixels[i*largura+j];
			pixels[i*largura+j] = val;
		}
	}
	
	delete [] tamanhoPixels;
}

void imagemPgm::salvaImagem(char novoNome []){
	
	int i,j;
	unsigned char * tamanhoPixels;
	ofstream saida (novoNome);
	
	tamanhoPixels = (unsigned char *) new unsigned char[altura*largura];
	
	int val;
	
	for(i=0; i<altura; i++)
	{
		for(j=0;j<largura;j++)
		{
			val = pixels[i*largura+j];
			tamanhoPixels[i*largura+j] = (unsigned char)val;
		}
	}
	
	if(!saida.is_open())
	{
		cout << "Nao se pode abrir arquivo de saida " << novoNome << endl;
		exit(1);
	}
	
	saida << "P5" << endl;
	saida << altura << " " << largura << endl;
	saida << 255 << endl;
	
	saida.write(reinterpret_cast<char *>(tamanhoPixels), (altura*largura)*sizeof(unsigned char));
	
	saida.close();
}

int imagemPgm::getPixel (int i,int j)
{
		return pixels[i*getLargura()+j];
}

void imagemPgm::alteraPixel (int i,int j,int novoPixel)
{
		pixels[i*getLargura()+j]=novoPixel;
}
