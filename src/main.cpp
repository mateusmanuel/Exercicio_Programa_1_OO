#include <iostream>

#include "negativo.hpp"
#include "sharpen.hpp"
#include "smooth.hpp"
#include "imagemPgm.hpp"

using namespace std;

int main(){
	imagemPgm imagemLena ("lena.pgm"); // Verificando lena.pgm
	negativo filtroNegativo;
	sharpen filtroSharpen;
	smooth filtroSmooth;
		
	imagemLena.leiaImagem("lena.pgm");
	
	cout << "Aplicando o filtro Negativo na imagem..."	<< endl;
	filtroNegativo.aplicaFiltro(imagemLena);
	cout << "Imagem negativada gerada!"	<< endl << endl;
	imagemLena.salvaImagem("lena_negativo.pgm");
	
	cout << "Aplicando o filtro Smooth na imagem..." << endl;	
	filtroSmooth.aplicaFiltro(imagemLena);
	cout << "Imagem com filtro Smooth gerada!"	<< endl << endl;
	imagemLena.salvaImagem("lena_smooth.pgm");
	
	cout << "Aplicando o filtro Sharpen na imagem..." << endl;	
	filtroSharpen.aplicaFiltro(imagemLena);
	imagemLena.salvaImagem("lena_sharpen.pgm");
	cout << "Imagem com filtro Sharpen gerada!"	<< endl << endl;
		
	return 0;
}
