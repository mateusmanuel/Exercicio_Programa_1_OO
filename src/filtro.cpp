#include "filtro.hpp"
#include "imagemPgm.hpp"

void filtro::colocaFiltro (int vetorFiltro [], imagemPgm algumaImagemPgm)
{
	int divisor, tamanho, metade, num, i, j, x, y;
	int * aux = new int[algumaImagemPgm.getAltura()*algumaImagemPgm.getLargura()];
		
	divisor = 1;
	tamanho = 3;
	metade = tamanho/2;
	
	for(i=metade; i<algumaImagemPgm.getAltura()-metade; i++)
	{
		for(j=metade; j<algumaImagemPgm.getLargura()-metade; j++)
		{
			num=0;
			
			for(x=-1;x<=1;x++)
			{
				for(y=-1;y<=1;y++)
				{
					num += vetorFiltro[(x+1)+tamanho*(y+1)]*algumaImagemPgm.getPixel(i+x,y+j);
				}
			}
		
			num=num/divisor;
		
			num=num < 0 ? 0 : num;
			num=num > 255 ? 255: num;
		
			aux[i+algumaImagemPgm.getLargura()*j]=num;
		}
	}

	for(i=0; i<algumaImagemPgm.getAltura(); i++)
	{
		for(j=0; j<algumaImagemPgm.getLargura(); j++)
		{
			algumaImagemPgm.alteraPixel (i,j,aux [i+algumaImagemPgm.getLargura()*j]);
		}
	}
}
