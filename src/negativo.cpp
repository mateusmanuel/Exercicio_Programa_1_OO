#include "negativo.hpp"

void negativo::aplicaFiltro(imagemPgm &algumaImagemPgm) {
	int i,j,aux;
	
	for(i=0; i<algumaImagemPgm.getAltura(); i++)
	{
		for(j=0; j<algumaImagemPgm.getLargura(); j++)
		{
			aux = 255 - algumaImagemPgm.getPixel(i,j);
			algumaImagemPgm.alteraPixel(i,j,aux);
		}
	}
			
}


