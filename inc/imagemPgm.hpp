#ifndef IMAGEMPGM_H
#define IMAGEMPGM_H
	
class imagemPgm {
	
	private:
		char numeroMagico[5];
		int altura;
		int largura;
		int nivelCinza;
		int *pixels;
		
	public:
		//Métodos Contrutores
		imagemPgm ();
		imagemPgm (int altura, int largura, int nivelCinza);
		imagemPgm (char nomeArquivo[]);
		
		//Outros Métodos
		int getAltura ();
		int getLargura ();
		int getNivelCinza ();
		void setAltura (int altura);
		void setLargura (int largura);
		void setNivelCinza (int valCinza);
		void leiaImagem (char nomeArquivo[]);
		void salvaImagem (char novoNome[]);
		int getPixel (int i,int j);
		void alteraPixel (int i,int j,int novoPixel);
};	
		
#endif
