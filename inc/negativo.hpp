#ifndef NEGATIVO_H
#define NEGATIVO_H

#include "filtro.hpp"

class negativo : public filtro {
	
	public:
		void aplicaFiltro(imagemPgm &algumaImagemPgm);

};

#endif
