#ifndef SMOOTH_H
#define SMOOTH_H

#include "filtro.hpp"

class smooth : public filtro {
	
	public:
		void aplicaFiltro (imagemPgm &algumaImagemPgm);	

};

#endif
