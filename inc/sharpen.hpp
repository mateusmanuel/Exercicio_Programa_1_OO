#ifndef SHARPEN_H
#define SHARPEN_H

#include "filtro.hpp"

class sharpen : public filtro {
	
	public:
		void aplicaFiltro (imagemPgm &algumaImagemPgm);	

};		

#endif
