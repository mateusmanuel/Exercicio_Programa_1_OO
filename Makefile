BINFOLDER := bin/
INCFOLDER := inc/
SRCFOLDER := src/
OBJFOLDER := obj/

CCP := g++
CFLAGS := -Wall -ansi

SRCFILES := $(wildcard src/*.cpp)

all: $(SRCFILES:src/%.cpp=obj/%.o)
	$(CCP) $(CFLAGS) obj/*.o -o bin/finalBinary -I./inc
	
obj/%.o: src/%.cpp
	$(CCP) $(CFLAGS) -c $< -o $@ -I./inc
	
.PHONY: clean

run: 
	 ./bin/finalBinary

clean:
	rm -rf obj/*
	rm -rf bin/*
	rm lena_negativo.pgm lena_smooth.pgm lena_sharpen.pgm

	
