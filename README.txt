PASSOS PARA A EXECUÇÃO DO PROGRAMA

1. Coloque a imagem lena.pgm na pasta principal do projeto.

2. Digite o comando "make"  para compilar e gerar o executável.

3. Digite o comando "make run" para executar o binário final.

4. Digite o comando "make clean" se desejar excluir os arquivos objetos, o binário final e as imagens com os filtros.

